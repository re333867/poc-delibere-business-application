package test.delibere.poc.service.controller;

import java.io.StringWriter;

import org.jbpm.services.api.DefinitionService;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.UserTaskService;
import org.jbpm.services.api.admin.ProcessInstanceMigrationService;
import org.jbpm.services.api.query.QueryService;
import org.kie.server.services.api.KieServer;
import org.kie.server.services.api.KieServerRegistry;
import org.kie.server.springboot.jbpm.ContainerAliasResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;


@RestController
public class JBPMProcessController {

	@Autowired
	private ProcessService processService;

	@Autowired
	private RuntimeDataService runtimeDataService;

	@Autowired
	private UserTaskService userTaskService;;

	@Autowired
	private DeploymentService deploymentService;

	@Autowired
	private KieServerRegistry registry;

	@Autowired
	private DefinitionService definitionService;

	@Autowired
	private QueryService queryService;

	@Autowired
	private ProcessInstanceMigrationService processInstanceMigrationService;

	@Autowired
	private KieServer kieServer;

	@Autowired
	private ContainerAliasResolver aliasResolver;

	@GetMapping(value = "/avviaWF", produces = "application/json")
	@ApiOperation(value = "Avvia WF delibera")
	public ProcessoResource avviaWF() {

		StringWriter st = new StringWriter();
		deploymentService.getDeployedUnits().forEach(du -> {
			st.append(du.getDeploymentUnit().getIdentifier()).append(du.getDeploymentUnit().getStrategy().name());
		});
											  	
		Long id = processService.startProcess("poc-delibere-business-application-kjar-1_0-SNAPSHOT",
				"TestProcesso1");
		 
		ProcessoResource t = new ProcessoResource();
		t.setIdProcesso(id.toString());
		t.setProcessName("XXXXXXX");
		return t;
	}

}
